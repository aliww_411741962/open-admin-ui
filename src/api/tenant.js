import request from '@/libs/request'

/**
 * 获取分页数据
 */
const list = ({page, limit}) => {
    const params = {page: page, limit: limit}
    return request({
        url: '/base/tenant/list',
        params,
        method: 'get'
    })
}

/**
 * 根据ID查找数据
 */
const get = (id) => {
    const params = {
        id: id
    }
    return request({
        url:'/base/tenant/get',
        params,
        method: 'get'
    })
}

/**
 * 添加数据
 */
const add = (data) => {
    return request({
        url: '/base/tenant/add',
        data,
        method: 'post'
    })
}

/**
 * 更新数据
 */
const update = (data) => {
    return request({
        url: '/base/tenant/update',
        data,
        method: 'post'
    })
}

/**
 * 删除数据
 * @param apiId
 */
const remove = (id) => {
    const data = {
        id: id
    }
    return request({
        url: '/base/tenant/remove',
        data,
        method: 'post'
    })
}
/**
 * 获取租户角色
 * @param tenantId
 */
export const getTenantRoles = (tenantId) => {
  const params = {
    tenantId: tenantId
  }
  return request({
    url: '/base/tenant/getRolesByTenantId',
    params,
    method: 'get'
  })
}
/**
 * 分配用户角色
 * @param data
 */
export const addTenantRoles = ({ tenantId, grantRoles }) => {
  const data = { tenantId: tenantId, roleIds: grantRoles.join(',') }
  return request({
    url: 'base/tenant/roles/add',
    data,
    method: 'post'
  })
}
export default {
    list,
    get,
    add,
    update,
  getTenantRoles,
  addTenantRoles,
    remove
}
